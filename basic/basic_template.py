"""
realpython.com
https://realpython.com/python-data-visualization-bokeh/#using-the-columndatasource-object
"""

"""
Bokeh Visualization Template

This template is a general outline for turning your data into a
visualization using Bokeh.
"""
import os
# Data handling
import pandas as pd
import numpy as np

# Bokeh libraries
from bokeh.io import output_file, output_notebook
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource
from bokeh.layouts import row, column, gridplot
from bokeh.models.widgets import Tabs, Panel

path_html = os.path.join(os.path.abspath("."), "basic", "name.html")

# Determine where the visualization will be rendered
output_file(path_html)  # Render to static HTML, or

# Set up the figure(s)
fig = figure()  # Instantiate a figure() object

# Preview and save
show(fig)  # See what I made, and save if I like it
