# python_visualization_with_bokeh

As part of an exercise at FH JOANNEUM, this small tutorial was created for students who have no experience with Bokeh.

![](do_not_touch/LoL.jpg)

## Introduction
Since I don't want to reinvent the wheel and there have been many people who have summarized this topic I will provide a best of collection here and link the references below.

The idea is that the user can come back at a later time and use this repo as a help or even as a starting point for his own project.

## Setup ENV
- clone repo
- check that your python is 3.8 or higher
- make a simple virtual env and load all packages needed from the requirements.txt

*Setup on Windows (git bash)*
```{bash}
git clone https://gitlab.com/laszloBalo/python_visualization_with_bokeh.git
cd python_visualization_with_bokeh
python -V
python -m venv venv
source venv/Scripts/activate
python -m pip install --upgrade pip
pip install -r requirements.txt
```
*Mac/Linux - replace the following line*
```{bash}
source venv/bin/activate
```

## Basic
No matter if the user wants to display his plots in Jupyter or in the browser ... all this is possible with Bokeh

Have a look at the introductory examples in the **basic** folder

## Funny Example
A little more advanced example by
bradtraversy

## Stream
A simple data stream [example](https://github.com/bokeh/bokeh/tree/branch-2.3/examples/app/ohlc)

## Advanced
An example in Bokeh which shows a cool interactive page

Check out the **advanced** folder


*setup server*
```{bash}
cd advanced
bokeh serve main.py
```

## Links
If you are more interested in interesting articles about Bokeh you should have a look at the following links:
- [python - venv](https://docs.python.org/3/tutorial/venv.html)
- [bokeh](https://bokeh.org/)
- [realpython - bokeh](https://realpython.com/python-data-visualization-bokeh/)
- [holoviz](https://holoviz.org/index.html)
- Data Visualization with Bokeh in Python (towardsdatascience)
    - [Part1](https://towardsdatascience.com/data-visualization-with-bokeh-in-python-part-one-getting-started-a11655a467d4)
    - [Part2](https://towardsdatascience.com/data-visualization-with-bokeh-in-python-part-ii-interactions-a4cf994e2512)
    - [Part3](https://towardsdatascience.com/data-visualization-with-bokeh-in-python-part-iii-a-complete-dashboard-dc6a86aa6e23)
- [Plotting Bokeh, an Analysis of its Architectural Variables](https://desosa.nl/projects/bokeh/2020/03/19/vision-to-architecture.html)
- [All Visualization Techniques used in Python](https://medium.com/towards-artificial-intelligence/all-visualization-techniques-used-in-python-602b14096d)
- [Why HoloViz](https://holoviz.org/background.html)
- [Next Level Data Visualization — Dashboard App with Bokeh & Flask](https://towardsdatascience.com/https-medium-com-radecicdario-next-level-data-visualization-dashboard-app-with-bokeh-flask-c588c9398f98)
- [Python Bokeh Data Visualization Tutorial](https://www.journaldev.com/19527/bokeh-python-data-visualization#52-multiple-plots)
- [Python Bokeh scatter plot](https://www.educative.io/edpresso/python-bokeh-scatter-plot)
- [Tut on basic Plotting with Bokeh](https://github.com/groundhogday321/python-bokeh/blob/master/Python%20Bokeh.ipynb)
- [Python Data Visualization With Bokeh](https://www.youtube.com/watch?v=2TR_6VaVSOs&t=47s)
- [Streaming Example](https://github.com/bokeh/bokeh/tree/branch-2.3/examples/app/ohlc)